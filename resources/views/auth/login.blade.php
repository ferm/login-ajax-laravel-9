@extends('layouts.app')
@section('content')
<script>
var base_url = "{{url('/')}}";
</script>
<div class="container mt-5">
	<div class="row justify-content-center">
		<div class="col-md-5 mt-5">
			<form class="login" method="post" enctype="multipart/form-data" id="form-login" name="form-login" accept-charset="utf-8">
				@csrf
				<div class="card">
					<div class="card-body">
						<h5 class="text-center card-title mb-4">
							Inicio de Sesión
						</h5>
						<div id="div-cnt-login" class="row">
							<div class="col-12" id="div-cnt-msg-login"></div>
							<div class="col-md-12 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input type="email" class="form-control float-form @error('email') is-invalid @enderror" placeholder=" " required="required" autocomplete="off" id="email" name="email" value="{{old('email')}}"/>
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Usuario</label>
										<i class="fa fa-user form-control-feedback"></i>
									</span>
								</div>
							</div>
							
							<div class="col-md-12 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input type="password" placeholder=" " class="form-control float-form @error('password') is-invalid @enderror" id="password" name="password" size="30" autocomplete="off" required="required">
										<label for="password">Contraseña</label>
										<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							</div>

							<div class="col-6">
								<a class="align-middle text-muted" href="{{route('password.request')}}" id="a-to-recover-passwd">
									Recupear contraseña? <i class="fa fa-chevron-right"></i>
								</a>
							</div>

							<div class="col-6 align-content-end justify-content-end">
							</div>
							<div class="col-7">
								<div class="custom-control custom-checkbox mt-1 float-end">
									<input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="remember">Recordarme</label>
								</div>
							</div>
							<div class="col-5">
								<button type="submit" class="btn btn-primary btn-block" id="btn-login">
									<i class="fa fa-sign-in"></i> Iniciar
								</button>
							</div>
							<div class="col-md-12 mt-3 mb-2 text-center">
								<a class="" href="{{url('register')}}"><i class="fa fa-plus"></i> Registráte</a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{asset('public/js/ajxlogin.js')}}" defer></script>
<script>
$(document).on("submit", ".login", function (event) {
	var data = $(this).serialize();
	$.ajax({
		dataType:   "JSON",
		type:       "post",
		url:        base_url+"/loginajx",
		data:       data,
		cache:      false,
		beforeSend: function(datos){
			$("#btn-login").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Validando...');
			$("#btn-login").attr("disabled", true);
		},
		success: function (datos){
			if (datos.type == "danger") {
				$("#div-cnt-msg-login").html('<div class="alert alert-danger alert-dismissible" role="alert"> <i class="fa fa-exclamation-circle"></i> '+datos.msg+
					'</div>');
				$("#btn-login").attr("disabled", false);
				$("#btn-login").html('<i class="fa fa-sign-in"></i> Iniciar sesión');
				setTimeout(function(){
					$("#div-cnt-msg-login").html('');
				}, 3000);
			}
			if (datos.type == "success") {
				$("#div-cnt-msg-login").html('<div class="alert alert-success text-center" role="alert"><i class="fa fa-check"></i> '+datos.msg+'</div>');
				$("#div-cnt-login").html('<div class="col-12 text-center text-muted"><h4><div class="spinner-border text-red spin-3x" role="status"><span class="sr-only">Loading...</span></div> <br>Redireccionando...</h4></div>');
				setTimeout(function(){
					$(window).attr('location', base_url+'/');
				}, 1000);
			}
		},
		error: function (data){
			$("#btn-login").html('<i class="fa fa-sign-in"></i> Iniciar');
			$("#btn-login").attr("disabled", false);
		}
	});
	event.preventDefault();
});
</script>
@endsection