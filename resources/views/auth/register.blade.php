@extends('layouts.app')
@section('content')
<script>
var base_url = "{{url('/')}}";
</script>
<div class="container mt-5">
	<div class="row justify-content-center">
		<div class="col-md-6 mt-5 mb-5">
			<div class="card">
				<div class="card-body">
					<h5 class="card-title text-center mb-4">Registro</h5>
					<!--form class="singup" method="POST" action="{{ route('register') }}"-->
					<form class="singup" method="post" enctype="multipart/form-data" id="form-singup" name="form-singup" accept-charset="utf-8">
						@csrf
						<div class="row">
							<div class="col-12" id="div-cnt-msg-singup"></div>
							<div class="col-12 col-sm-12 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="name" type="text" class="form-control float-form @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required="" autofocus="" autocomplete="off">
										@error('name')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="name">Nombre</label>
										<i class="fa fa-user form-control-feedback"></i>
									</span>
								</div>
							</div>

							<div class="col-12 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="email" type="email" class="form-control float-form @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required="" autocomplete="email">
										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="email">Email</label>
										<i class="fa fa-at form-control-feedback"></i>
									</span>
								</div>
							</div>

							<div class="col-12 col-sm-6 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password" type="password" class="form-control float-form @error('password') is-invalid @enderror" name="password" required="" autocomplete="new-password">
										@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<label for="password">Contraseña</label>
										<i id="icon-eye" class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password"></i>
									</span>
								</div>
							 </div>

							 <div class="col-12 col-sm-6 mb-3">
								<div class="form-group input-group">
									<span class="has-float-label">
										<input id="password-confirm" type="password" class="form-control float-form matches-passwd" name="password_confirmation" required autocomplete="new-password" data-passwd="password" data-confirm="password-confirm">
										<label for="password-confirm">Confirmar contraseña</label>
										<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="password-confirm"></i>
									</span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12 col-md-12 col-12">
								<a class="float-start" href="{{url('login')}}">
									<i class="fa fa-chevron-left"></i> Iniciar sesión
								</a>

								<button type="submit" class="btn btn-primary float-end" id="btn-singup">
									<i class="bi bi-check-circle"></i> Continuar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('resources/js/ajxlogin.js')}}"></script>
<script>
$(document).on("submit", ".singup", function (event) {
	var data = $(this).serialize();
	$.ajax({
		dataType:   "JSON",
		type:       "post",
		url:        base_url+"/singupajx",
		data:       data,
		cache:      false,
		beforeSend: function(datos){
			$("#btn-singup").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Validando...');
			$("#btn-singup").attr("disabled", true);
		},
		success: function (datos){
			$("#div-cnt-msg-singup").html('<div class="alert alert-danger alert-dismissible" role="alert"> <i class="'+datos.icon+'"></i> '+datos.msg+'</div>');
			$("#btn-singup").attr("disabled", false);
			$("#btn-singup").html('<i class="bi bi-check-circle"></i> Continuar');
			setTimeout(function(){
				$("#div-cnt-msg-singup").html('');
			}, 3000);
			if (datos.type=='success') {
				$("#form-singup")[0].reset();
			}
		},
		error: function (data){
			$("#btn-singup").html('<i class="bi bi-check-circle"></i> Continuar');
			$("#btn-singup").attr("disabled", false);
		}
	});
	event.preventDefault();
});
</script>
@endsection