@extends('layouts.app')
@section('content')
@section('title', 'Perfil')
<script>
	var base_url = "{{url('/')}}";
</script>
<div class="container mt-5">
	<div class="row">
		<div class="col-md-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}"><i class="fa fa-home"></i> Inicio</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page">Perfil de usuario</li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" aria-current="page" href="#profile" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" role="tab" aria-controls="profile" aria-selected="false">
						<i class="bi bi-person-circle"></i> <span class="d-none d-sm-inline-block">Perfil</span>
					</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" aria-current="page" href="#security" id="security-tab" data-bs-toggle="tab" data-bs-target="#security" role="tab" aria-controls="security" aria-selected="false">
						<i class="bi bi-lock"></i> <span class="d-none d-sm-inline-block">Seguridad</span>
					</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="card mt-3">
						<div class="card-header">
							Información de pérfil
						</div>
						<div class="card-body">
							<div class="row mt-3">
								<div class="col-md-3">
									<div class="row">
										<div id="div-img" class="col-md-12 mb-2"></div>
									</div>
									<form name="form-up-imgs-all" id="form-up-imgs-all" method="post" accept-charset="utf-8" enctype="multipart/form-data">
										@csrf
										<div class="row">
											<div class="col-md-12">
												<input type="file" class="form-control up-image-profile" id="files-all" name="images" required="required" accept=".png, .jpg, .jpeg">
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 mt-1">
												<div class="progress" style="display: none;">
													<div id="prg-bar-up-all" class="progress-bar bg-defult" role="progressbar" style="width:100%;">0%</div>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-9">
									<form id="form-up-account" name="form-up-account" action="" method="POST" action="">
										@csrf
										<div class="row">
											<div class="col-md-4">
												<div class="form-group input-group">
													<span class="has-float-label">
														<input type="email" class="form-control" name="email" id="txt-email" placeholder="Nombre" required="required" value="{{$profile->email}}" autocomplete="off" readonly="" disabled="">
														<label for="txt-email">Email</label>
													</span>
												</div>
											</div>
											<div class="col-md-8">
												<div class="form-group input-group">
													<span class="has-float-label">
														<input type="text" class="form-control" name="name" id="txt-name" placeholder="Nombre" required="required" value="{{$profile->name}}" autocomplete="off">
														<label for="txt-name">Nombre</label>
													</span>
												</div>
											</div>
											<div class="col-md-12 mt-3">
												<button id="btn-up-account" name="btn-up-account" type="submit" class="btn btn-primary float-end">
													<i class="bi bi-save"></i> Actualizar
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
					<div class="card mt-3">
						<div class="card-header">
							Información de pérfil
						</div>
						<div class="card-body">
							<form id="form-up-passwd" name="form-up-passwd" action="" method="POST">
								@csrf
								<div class="row mt-3 justify-content-center">
									<div class="col-md-4">
										<div class="form-group input-group">
											<span class="has-float-label">
												<input type="password" class="form-control" name="current" id="txt-current" placeholder="Nombre" required="required" autocomplete="off">
												<label for="txt-current">Contraseña Actual</label>
												<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="txt-current"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="row mt-3 justify-content-center">
									<div class="col-md-4">
										<div class="form-group input-group">
											<span class="has-float-label">
												<input type="password" class="form-control" name="password" id="txt-passwd" placeholder="Nombre" required="required" autocomplete="off" minlength="6">
												<label for="txt-passwd">Nueva Contraeña</label>
												<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="txt-passwd"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="row mt-3 justify-content-center">
									<div class="col-md-4">
										<div class="form-group input-group">
											<span class="has-float-label">
												<input type="password" class="form-control" name="confirmed" id="txt-passwd-confirm" placeholder="Nombre" required="required" autocomplete="off" minlength="6">
												<label for="txt-passwd-confirm">Confirmar Contraeña</label>
												<i class="fa fa-eye-slash form-control-feedback btn-show-passwd" data-passwd="txt-passwd-confirm"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="row mt-3 justify-content-center">
									<div class="col-md-4">
										<button id="btn-up-passwd" name="btn-up-passwd" type="submit" class="btn btn-primary float-end">
											<i class="bi bi-check-circle"></i> Actualizar
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{asset('public/js/accountajx.js')}}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
		loadImg();
	});
</script>
@endsection