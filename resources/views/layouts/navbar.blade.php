<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Eighth navbar example">
	<div class="container">
		<a class="navbar-brand" href="{{url('/')}}">
			<img class="img-fluid" src="{{asset('public/app/images/logo_navbar.svg')}}" id="logo_custom"  alt="logo" width="100">
		</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" onclick="sitepoint.SidebarNav_toggle(event);" onkeyup="sitepoint.SidebarNav_toggle(event);">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExample07">
			<ul class="navbar-nav flex-row flex-wrap ms-md-auto">
				<li class="nav-item col-6 col-md-auto">
					<a class="nav-link {{request()->is('/')?'active':''}}" href="{{url('/')}}">Inicio</a>
				</li>
				<li class="nav-item col-6 col-md-auto">
					<a class="nav-link" href="https://linuxitos.com/blog/about">About</a>
				</li>
				<li class="nav-item col-6 col-md-auto">
					<a class="nav-link" href="https://linuxitos.com/blog/servicios">Services</a>
				</li>
				<li class="nav-item col-6 col-md-auto">
					<a class="nav-link" href="https://linuxitos.com/blog/contacto">Contact</a>
				</li>
				<li class="nav-item col-6 col-md-auto dropdown desplegable">
					<a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-bs-toggle="dropdown" aria-expanded="false">Tutoriales</a>
					<div class="dropdown-menu dropdown-menu-end desplegable-menu" aria-labelledby="dropdown07">
						<a class="dropdown-item" href="{{url('pageajax')}}">
							Paginación con ajax
						</a>
						<a class="dropdown-item" href="{{url('pagenotajax')}}">
							Paginación sin ajax
						</a>
						<a class="dropdown-item" href="{{url('scroll')}}">
							Scroll
						</a>
						<a class="dropdown-item" href="{{url('docs')}}">
							Docs
						</a>
					</div>
				</li>

				@if (auth()->check())
					<li class="nav-item col-6 col-md-auto dropdown desplegable">
						<a class="nav-link dropdown-toggle" href="#" id="menu-options" data-bs-toggle="dropdown" role="button" aria-expanded="false">
							<span class="pull-left">
								<img src="{{(auth()->user()->image!='' && file_exists(public_path(auth()->user()->files.auth()->user()->image)) ? asset('public/'.auth()->user()->files.auth()->user()->image): asset('public/images/default.svg'))}}" class="img-circle rounded-circle bg-white" title="Imagen user" alt="Imagen user" width="30px" height="30px"> {{auth()->user()->name}}
							</span>
							<span class="caret"></span>
						</a>
						<div class="dropdown-menu dropdown-menu-end desplegable-menu div-menu" aria-labelledby="menu-options">
							<a class="dropdown-item {{request()->is('account')?'active':''}}" href="{{route('account.index')}}">
								Perfil <i class="bi bi-person float-end"></i>
							</a>
							<a class="dropdown-item" href="{{url('ptables')}}">
								Tablas con Ajax <i class="bi bi-table float-end"></i>
							</a>
							<a class="dropdown-item" href="{{url('ptablesnjx')}}">
								Tablas sin Ajax <i class="bi bi-table float-end"></i>
							</a>
							<a class="dropdown-item" href="{{url('upimages')}}">
								Subir imágenes <i class="bi bi-images float-end"></i>
							</a>
							<a class="dropdown-item" href="{{url('dragdrop')}}">
								Drag & Drop <i class="bi bi-files float-end"></i>
							</a>
							<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								Salir <i class="bi bi-box-arrow-right float-end"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
						</div>
					</li>
				@endif
			</ul>
			@guest
				@if(!request()->is('register'))
					<a class="my-2 my-md-0 ms-md-3 me-2 btn btn-outline-warning" href="{{url('register')}}">
						Registro
					</a>
				@endif
				@if(!request()->is('login'))
					<a class="btn btn-outline-primary" href="{{url('login')}}">
						Iniciar Sesión
					</a>
				@endif
			@endguest
		</div>
	</div>
</nav>