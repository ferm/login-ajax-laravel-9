<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="text-center">
					Copyright @2020 | Designed & Development by <a href="https://linuxitos.com/">LuNuXiToS</a>
				</p>
			</div>
		</div>
	</div>
</footer>