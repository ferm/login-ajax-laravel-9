@section('title', 'Inicio')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="{{ asset('public/app/images/favicon.png') }}"/>
		<title>{{ config('app.name', 'Hogar') }} - @yield('title')</title></title>
		<!-- CSS only -->
		<link rel="stylesheet" href="{{asset('public/vendor/bootstrap/css/animate.css')}}">
		<link rel="stylesheet" href="{{asset('public/vendor/bootstrap/css/animation.css')}}">
		<link rel="stylesheet" href="{{asset('public/vendor/bootstrap-icons/bootstrap-icons.css')}}">
		<link rel="stylesheet" href="{{asset('public/vendor/bootstrap/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('public/vendor/font-awesome/css/all.css')}}">
		<link href="{{asset('public/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('public/app/css/custom.css')}}" rel="stylesheet">
		<script src="{{asset('public/vendor/jquery/jquery-3.6.0.min.js')}}"></script>
		<script src="{{asset('public/vendor/notify/notify-custom.js')}}"></script>
		<link rel="stylesheet" href="{{asset('public/vendor/lightbox/lightbox.css')}}">
		<meta name="csrf-token" content="{{ csrf_token() }}"/>
	</head>
	<body>
		@include('layouts.navbar')
		<div id="app">
			<main class="py-4">
				@yield('content')
			</main>
		</div>

		@include('layouts.footer')

		<script>
			window.jQuery || document.write('<script src="{{asset("public/vendor/jquery/jquery-3.6.0.min.js")}}"><\/script>')
		</script>
		<script src="{{asset('public/vendor/jquery/jquery-3.6.0.min.js')}}"></script>

		<!-- Scripts for bootstrap 4.3.1-->
		<!--script src="{{asset('public/vendor/bootstrap/js/feather.min.js')}}"></script-->
		<script src="{{asset('public/vendor/bootstrap/js/popper.min.js')}}"></script>
		<script src="{{asset('public/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('public/vendor/bootstrap/js/jquery.sharrre.js')}}"></script>
		<script src="{{asset('public/vendor/bootstrap/js/ie10-viewport-bug-workaround.js')}}"></script>
		<script src="{{asset('public/app/js/footer.js')}}"></script>
		<script src="https://www.google.com/recaptcha/api.js"></script>
		<script src="{{asset('public/vendor/mask/jquery.mask.min.js')}}"></script>

		<script src="{{asset('public/vendor/lightbox/lightbox.js')}}"></script>
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="{{asset('public/vendor/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
		<script src="{{asset('public/vendor/bootstrap-notify/fun_msg.js')}}"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$(window).scroll(function(){
					if($(this).scrollTop() > 100){
						$('#scroll').fadeIn();
					}else{
						$('#scroll').fadeOut();
					}
				});
				$('#scroll').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});

				var url = document.location.toString();
				if (url.match('#')) {
					$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
				}
				$('.nav-tabs a').on('shown.bs.tab', function (e) {
					let stateObj = { id: "100" };
					window.history.replaceState(stateObj,"Page 3", e.target.hash);
				});
			});
		</script>
		<script type="text/javascript">
			$('.dropdown .dropdown-menu').on('click', function(event){
				event.stopPropagation();
			});
			$('.no-cerrar .dropdown-menu').on('click', function(event){
				event.stopPropagation();
			});
		</script>
	</body>
</html>