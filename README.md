# Login Ajax Laravel 9

![alt tag](./public/images/1.png)

Un proyecto de inicio de sesión, registro, perfil y cambio de contraseña, usando laravel y ajax

## Descripción

Éste proyecto es el inicio de una serie de características que quiero añadirle, utiliazando la tecnología web ajax,
como se podrá apreciar, es un proyecto de ejemplo, de uso para pruebas locales, y sobre todo para ir aprendiendo
para los que apenas se inicien en ajax con laravel.

Aquí se presenta una idea de cómo implementar dichas tecnologias, sin embargo no son definitivas, están sujetas a
mejoras, como en todo.

Características de la parte 1
================
- Boostrap 5.1.3
- Laravel 9
- Uso de ajax
- Inicio de sesión con ajax
- Registro de usuario con ajax
- Perfil actualización de datos con ajax
- Cambio de contraseña con ajax

Uso
=====
1.- Configuar el archivo .env con la url de su localhost, y conexión a BD
2.- Crear la base de datps en XAMPP, o su servidor php que usen
3.- Clonar proyecto y correr las migraciones

	git clone git@gitlab.com:ferm/login-ajax-laravel-9.git
	php artisan migrate

4.- Acceder a la url que se uso para el proyecto.

	http://localhost/laravel-one/

Visitar https://blog.linuxitos.com/post/inicio-de-sesion-registro-con-ajax-y-laravel-9

# Capturas

![alt tag](./public/images/1.png)
![alt tag](./public/images/2.png)
![alt tag](./public/images/3.png)
![alt tag](./public/images/4.png)
![alt tag](./public/images/5.png)
![alt tag](./public/images/6.png)

#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>