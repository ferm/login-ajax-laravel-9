<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\PostController::class, 'index'])->name('home');
/*Route::get('/', function () {
    return view('welcome');
});*/

Route::resource('posts', PostController::class);
Auth::routes();
Route::post('/loginajx', [App\Http\Controllers\Auth\CustomLoginController::class, 'loginUser']);
Route::post('/singupajx', [App\Http\Controllers\Auth\CustomSingupController::class, 'registerUser']);

Route::group(['middleware' => 'auth'], function () {
	Route::post('loadimage', [AccountController::class, 'loadImage']);
	Route::post('upimage', [AccountController::class, 'upImage']);
	Route::post('upprofile', [AccountController::class, 'update']);
	Route::post('delimage', [AccountController::class, 'delImage']);
	Route::post('uppassword', [AccountController::class, 'upPassword']);
	Route::resource('account', AccountController::class, ['parameters' => ['account' => 'user']]);
});
