<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CustomSingupController extends Controller
{
    public function registerUser(Request $request)
    {
        // now we get our form data from Request
        $data['name']       = $request->name; // request coming from ajax
        $data['email']      = strtolower($request->email); // request coming from ajax
        $data['password']   = $request->password; // request comming from ajax
        $status             = "active"; // this can be use to check if user account is activated

        // note Laravel uses an encryption system called bcrypt
        // this has been the secure foundation for login queries 
        // so here we are to encrypt as Laravel will accept when doing Aut

        // do other process
        /*$users = new Member();
        $users->email    = $email;
        $users->password = $hash_password;
        $users->status   = $status;
        $users->save();*/

        if (User::where('email',$data['email'])->count()==0) {
            User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            $msg = array("type" => 'success',
                        "icon"  => 'bi bi-check-circle',
                        "msg"   => 'Usuario registrado, ahora inicia sesión.');
        }else{
            $msg = array("type" => 'success',
                        "icon"  => 'bi bi-x-circle',
                        "msg"   => 'El email ya se ecuentra registrado.');
        }
        return response()->json($msg);
    }
}
