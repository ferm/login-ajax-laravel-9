<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class CustomLoginController extends Controller
{
	use AuthenticatesUsers;

	public function loginUser(Request $request){
		$email         = $request->email;
		$password      = $request->password;
		$rememberToken = $request->remember;
		$credentials = $request->validate([
			'email' => ['required', 'email'],
			'password' => ['required'],
        ]);
		// now we use the Auth to Authenticate the users Credentials
		// Attempt Login for members
		//if (Auth::guard('user')->attempt(['email' => $email, 'password' => $password], $rememberToken)) {
		//if (Auth::attempt($credentials)) {
		if (Auth::attempt(['email' => $email, 'password' => $password, 'status'=>'active'], $rememberToken)) {
			//$request->session()->regenerate();
			$msg = array("type" => 'success',
						"icon" 	=> 'bi bi-check-circle',
						"msg" 	=> 'Redireccionando');
			return response()->json($msg);
		}else{
			$msg = array("type" => 'danger',
						"icon" 	=> 'bi bi-check-circle',
						"msg" 	=> 'Usuario o contraseña incorrectos');
			return response()->json($msg);
		}
	}
}
