<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Posts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Str;

class AccountController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$userId     = auth()->user()->id;
		$profile    = User::find($userId);
		if ($profile->files=="") {
			$input['files'] = 'files/'.Str::random(7).$userId.'/';
			$todoStatus         = $profile->update($input);
		}
		$path = public_path().'/'.$profile->files;
		if(!File::exists($path)) {
			File::makeDirectory($path, $mode = 0775, true, true);
		}
		return view('profile', compact('profile'));
	}

	public function upImage(Request $request){
		$profile 	= auth()->user();
		$delImg 	= public_path().'/'.$profile->files.$profile->image;
		if($request->hasfile('images')){
			$up = false;
			$image = $request->file('images');
			$imageName 			= Str::random(4).'-'.Str::random(4).'.'.$image->extension();
			$image->move(public_path().'/'.$profile->files, $imageName);
			$input['image']     = $imageName;
			if($profile->update($input)){
				$up = true;
			}
			if ($up) {
				if(File::exists($delImg)){
					File::delete($delImg);
				}
				$msg = array("type" => 'success',
							"icon" 	=> 'bi bi-check-circle',
							"msg" 	=> 'Archivo subido correctamente.');
			}else{
				$msg = array("type" => 'danger',
							"icon" 	=> 'bi bi-x-circle',
							"msg" 	=> 'Error en la subida del archivo.');
			}
		}else{
			$msg = array("type" => 'danger',
						"icon" 	=> 'fa fa-times',
						"msg" 	=> 'Seleccione al menos una imagen.');
		}
		return response()->json($msg);
	}

	public function delImage(Request $request){
		$profile 	= auth()->user();
		$delImg 	= public_path().'/'.$profile->files.$profile->image;
		$input['image']     = null;
		if($profile->update($input)){
			if(File::exists($delImg)){
				File::delete($delImg);
			}
			$msg = array("type" => 'success',
						"icon" 	=> 'bi bi-check-circle',
						"msg" 	=> 'Imagen eliminada.');
		}else{
			$msg = array("type" => 'danger',
						"icon" 	=> 'bi bi-x-circle',
						"msg" 	=> 'Error interno, intenta más tarde.');
		}
		return response()->json($msg);
	}

	public function upPassword(Request $request){
		$profile 	= auth()->user();
		$validator = $request->validate([
			'current' => ['required'],
			'password' => ['required'],
			'confirmed' => ['same:password'],
		]);
		if ($validator) {
			$validate_admin = DB::table('users')
							->select('*')
							->where('id', auth()->user()->id)
							->first();
			if ($validate_admin && Hash::check($request->input('current'), $validate_admin->password)) {
				$pass['password'] 	= Hash::make($request->input('password'));
				if ($profile->update($pass)) {
					$msg = array("type" => 'success',
								"icon" 	=> 'fa fa-check',
								"msg" 	=> 'Contraseña actualizada.');
				} else {
					$msg = array("type" => 'danger',
								"icon" 	=> 'fa fa-times',
								"msg" 	=> 'Error con la base de datos.');
				}
			}else{
				$msg = array("type" => 'danger',
						"icon" 	=> 'fa fa-times',
						"msg" 	=> 'La contraseña actual no es correcta.');
			}
		}else{
			$msg = array("type" => 'danger',
						"icon" 	=> 'fa fa-times',
						"msg" 	=> 'Las contraseñas no coinciden.');
		}
		return response()->json($msg);
	}

	public function loadImage(){
		$profile 	= auth()->user();
		if ($profile->image!="" && File::exists(public_path().'/'.$profile->files.$profile->image)) {
			$img = asset('public/'.$profile->files.$profile->image);
		}else{
			$img = asset('public/images/default.svg');
		}
		$result['img'] = '<div class="box-img-evt">
						<a href="'.$img.'" data-lightbox="img-gallery-1" data-title="Imagen de perfil">
							<img class="img-fluid" src="'.$img.'" alt="Imagen de perfil"/>
						</a>
						<ul class="icon">
							<li>
								<a href="'.$img.'" class="btn btn-primary btn-sm" data-lightbox="img-gallery-2" data-title="Imagen">
									<i class="bi bi-eye"></i>
								</a>
							</li>';

		if (filled($profile->image)) {
			$result['img'] .= '<li>
							<button id="btn-del-img" type="button" class="btn btn-danger btn-sm del-image">
								<i class="bi bi-trash"></i>
							</button>
						</li>';
		}

		$result['img'] .= '</ul></div>';
		
		//public_path().'/'.$profile->files;
		return response()->json($result);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function edit(User $user)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, User $user)
	{
		$profile 	= auth()->user();
		if ($profile->update($request->all())) {
			$msg = array("type" => 'success',
						"icon" 	=> 'fa fa-check',
						"msg" 	=> 'Actualizado correctamente.');
		} else {
			$msg = array("type" => 'danger',
						"icon" 	=> 'fa fa-times',
						"msg" 	=> 'Error con la base de datos.');
		}
		return response()->json($msg);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		//
	}
}
