$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

$("#form-up-account").submit(function( e ) {
	$('#btn-up-account').attr("disabled", true);
	var data = $(this).serialize();
	var url = base_url+"/upprofile";
	$.ajax({
		url: 	url,
		method: 'POST',
		dataType: 'JSON',
		data: 	data,
		beforeSend: function(objeto) {
			$("#btn-up-account").html('<span class="spinner-border spin-x align-middle" role="status"><span class="sr-only">Loading...</span></span> Actualizando...');
		},
		success:function(response){
			notify_msg(response.icon, " ", response.msg, "#", response.type);
			$('#btn-up-account').attr("disabled", false);
			$("#btn-up-account").html('<i class="fa fa-check"></i> Actualizar');
		},
		error:function(error){
			$('#btn-up-account').attr("disabled", false);
			$("#btn-up-account").html('<i class="fa fa-check"></i> Actualizar');
			notify_msg("fa fa-exclamation-circle", " ", "Error interno, por favor intenta más tarde.", "#", "danger");
		}
	});
	e.preventDefault();
});

$(document).on("change", ".up-image-profile", function (event) {
	event.preventDefault();
	var data 	= new FormData();
	var fl 		= document.getElementById('files-all');
	var ln 		= fl.files.length;
	if (ln <= 0) {
		notify_msg("fa fa-exclamation-circle", " ", "Por favor seleccione al menos un archivo.", "#", "danger");
		return;
	}else{
		data.append('images', $('#files-all')[0].files[0]);
		$.ajax({
			url: 			base_url+"/upimage",
			data: 			data,
			contentType: 	false,
			cache: 			false,
			processData: 	false,
			type: 			'POST',
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						percentComplete = parseInt(percentComplete * 100);
						$("#prg-bar-up-all").text(percentComplete + '%');
						$("#prg-bar-up-all").css('width', percentComplete + '%');
					}
				}, false);
				return xhr;
			},
			beforeSend: function(objeto) {
				$("#prg-bar-up-all").removeAttr("class").attr("class", "progress-bar bg-success text-center");
				$("#prg-bar-up-all").css('width', '0');
				$("#btn-up-imgs-all").attr('disabled', 'disabled');
				$(".progress").show();
			},
			success: function (data) {
				notify_msg(data.icon, " ", data.msg, "#", data.type);
				$("#prg-bar-up-all").css('width', 100 + '%');
				$("#prg-bar-up-all").text('0%');
				$("#div-cnt-imgs-all").html("");
				if (data.type == 'success') {
					loadImg();
				}
				setTimeout(function(){
					$("#prg-bar-up-all").removeAttr("class").attr("class", "progress-bar bg-defult text-center");
					$(".progress").hide();
				}, 3000);
				$("#btn-up-imgs-all").attr("disabled", false);
				$("#form-up-imgs-all")[0].reset();
			},
			error: function(data) {
				$("#div-cnt-imgs-all").html("");
				$("#btn-up-imgs-all").attr("disabled", false);
				$("#prg-bar-up-all").css('width', 100 + '%');
				$("#prg-bar-up-all").text('0%');
				$("#prg-bar-up-all").removeAttr("class").attr("class", "progress-bar bg-danger text-center");
				notify_msg("fa fa-times", " ", "Error en la configuración de ajax.", "#", "danger");
				setTimeout(function(){
					$("#prg-bar-up-all").removeAttr("class").attr("class", "progress-bar bg-defult text-center");
					$(".progress").hide();
				}, 3000);
			}
		});
	}
});

function loadImg(){
	$.ajax({
		type: 		"POST",
		dataType: 	"JSON",
		method: 	"POST",
		url: 		base_url+"/loadimage",
		data: {
			id: "021",
		},
		beforeSend: function(objeto) {
			$("#div-img").html(' <div class="alert alert-info text-center"><span class="spinner-border spin-x align-middle" role="status" aria-hidden="true"></span>'+
				'<span class="sr-only">Loading...</span> Cargando</div>');
		},
		success: function(data) {
			$("#div-img").html(data.img);
		},
		error: function(error){
			$("#div-img").html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> Error, intente de nuevo.</div>');
		}
	});
}

$("#form-up-passwd").submit(function( e ) {
	$('#btn-up-passwd').attr("disabled", true);
	var data = $(this).serialize();
	var url = base_url+"/uppassword";
	$.ajax({
		url: 	url,
		method: 'POST',
		data: 	data,
		beforeSend: function(objeto) {
			$("#btn-up-passwd").html('<span class="spinner-border spin-x align-middle" role="status"><span class="sr-only">Loading...</span></span> Actualizando...');
		},
		success:function(response){
			notify_msg(response.icon, " ", response.msg, "#", response.type);
			$('#btn-up-passwd').attr("disabled", false);
			$("#btn-up-passwd").html('<i class="fa fa-check"></i> Actualizar');
			$("#form-up-passwd")[0].reset();
		},
		error:function(error){
			$('#btn-up-passwd').attr("disabled", false);
			$("#btn-up-passwd").html('<i class="fa fa-check"></i> Actualizar');
			notify_msg("fa fa-exclamation-circle", " ", "Error interno, por favor intenta más tarde.", "#", "danger");
		}
	});
	e.preventDefault();
});

function matchPasswd(passwd, confirm){
	var password = document.getElementById(passwd);
	var confirm_password = document.getElementById(confirm);
	function validatePassword(){
		if(password.value != confirm_password.value){
			confirm_password.setCustomValidity("Las contraseñas no coinciden.");
		}else{
			confirm_password.setCustomValidity('');
		}
	}
	password.onchange 			= validatePassword;
	confirm_password.onkeyup 	= validatePassword;
}

function readURL(input, div) {
	var total_files = $(input)[0].files.length;
	$("#"+div).html('');
	for (i = 0; i<total_files; i++) {
		if ($(input)[0].files[i]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$("#"+div).append('<div class="col-6"><img class="img-fluid" id="img-'+i+'" src="'+e.target.result+'"/></div>')
			};
			reader.readAsDataURL(input.files[i]);
		}
	}
}